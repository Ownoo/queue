﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace CallQueue.EndPoint.Controllers
{
    public class DummyController : ApiController
    {
        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public async Task<IHttpActionResult> Get()
        {
            await Task.Delay(random.Value.Next(100, 800));
            return Ok($"Hello at {DateTime.Now.ToString("HH:mm:ss.fff")}");
        }
    }
}
