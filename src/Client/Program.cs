﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Queue.Client
{
    class Program
    {
        static readonly int iterations = 50;
        static readonly int maxCPS = 5;
        static readonly int threads = 4;

        static readonly HttpClient client = new HttpClient();
        static readonly Queue<DateTime> calls = new Queue<DateTime>();
        static readonly object locking = new object();

        static async Task Main()
        {
            // prepare workload
            var workloads = Enumerable.Range(0, iterations).Select(o => new Workload
            {
                Id = o.ToString(),
                Url = "http://localhost:52836/Api/Dummy"
            });

            // queue for retrieval
            var queue = new ConcurrentQueue<Workload>(workloads);
            var tasks = Enumerable.Range(1, threads).Select(async _ =>
            {
                var output = new List<string>();
                while (queue.TryDequeue(out Workload workload))
                {
                    await Throttle(workload.Id);
                    lock (locking)
                    {
                        calls.Enqueue(DateTime.Now);
                    }
                    output.Add(await Request(workload));
                }
                return output;
            }).ToList();
            await Task.WhenAll(tasks);

            var fetches = tasks.SelectMany(t => t.Result).ToList();
            Console.ReadLine();
        }

        static async Task Throttle(string id)
        {
            TimeSpan delay = TimeSpan.Zero;
            lock (locking)
            {
                if (calls.Any())
                {
                    var dequeue = calls.Count(o => o.CompareTo(DateTime.Now.AddSeconds(-1)) < 0);
                    if (dequeue > 0)
                    {
                        Enumerable.Repeat(calls.Dequeue(), dequeue);
                    }
                    if (calls.Count() >= maxCPS)
                    {
                        delay = calls.Peek().Subtract(DateTime.Now.AddSeconds(-1));
                    }
                }
            }
            if (delay > TimeSpan.Zero)
            {
                await Task.Delay(delay);
                await Throttle(id);
            }
        }

        static async Task<string> Request(Workload workload)
        {
            Console.WriteLine($"{workload.Id} - fetch {DateTime.Now.ToString("HH:mm:ss.fff")}");
            using (var result = await client.GetAsync(workload.Url))
            {
                result.EnsureSuccessStatusCode();
                return await result.Content.ReadAsStringAsync();
            }
        }

        class Workload
        {
            public string Id { get; set; }
            public string Url { get; set; }
        }
    }
}
